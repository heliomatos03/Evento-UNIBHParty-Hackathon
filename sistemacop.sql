-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 10-Jun-2018 às 11:59
-- Versão do servidor: 5.6.13
-- versão do PHP: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `sistemacop`
--
CREATE DATABASE IF NOT EXISTS `sistemacop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sistemacop`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `agente`
--

CREATE TABLE IF NOT EXISTS `agente` (
  `id_agente` int(11) NOT NULL AUTO_INCREMENT,
  `id_instituto_fk` int(11) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `data_ultima_atualizacao` date NOT NULL,
  PRIMARY KEY (`id_agente`),
  KEY `id_instituto_fk` (`id_instituto_fk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `agente`
--

INSERT INTO `agente` (`id_agente`, `id_instituto_fk`, `latitude`, `longitude`, `data_ultima_atualizacao`) VALUES
(1, 1, '-19.9166813', '-44', '2018-06-10'),
(2, 1, '-19.865541', '-43.926388', '2018-06-08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `atendimento_ocorrencia`
--

CREATE TABLE IF NOT EXISTS `atendimento_ocorrencia` (
  `id_ocorrencia_fk` int(11) NOT NULL,
  `id_agente_fk` int(11) NOT NULL,
  `data_atendimento` date NOT NULL,
  KEY `id_ocorrencia_fk` (`id_ocorrencia_fk`),
  KEY `id_agente_fk` (`id_agente_fk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `camera`
--

CREATE TABLE IF NOT EXISTS `camera` (
  `id_camera` int(11) NOT NULL AUTO_INCREMENT,
  `descricao_camera` varchar(50) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `link_camera` varchar(200) NOT NULL,
  PRIMARY KEY (`id_camera`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicao`
--

CREATE TABLE IF NOT EXISTS `instituicao` (
  `id_instituicao` int(11) NOT NULL AUTO_INCREMENT,
  `nome_instituicao` varchar(50) NOT NULL,
  `descricao_instituicao` varchar(100) NOT NULL,
  PRIMARY KEY (`id_instituicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `instituicao`
--

INSERT INTO `instituicao` (`id_instituicao`, `nome_instituicao`, `descricao_instituicao`) VALUES
(1, 'PMMG', 'OS Peidados');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ocorrencia`
--

CREATE TABLE IF NOT EXISTS `ocorrencia` (
  `id_ocorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_ocorrencia_fk` int(11) NOT NULL,
  `id_instituicao_fk` int(11) NOT NULL,
  `descricao_ocorrencia` varchar(50) NOT NULL,
  `data_ocorrencia` date NOT NULL,
  `data_cadastro_ocorrencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endereco` varchar(150) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ocorrencia`),
  KEY `data_ocorrencia` (`data_ocorrencia`),
  KEY `data_ocorrencia_2` (`data_ocorrencia`),
  KEY `id_tipo_ocorrencia_fk` (`id_tipo_ocorrencia_fk`),
  KEY `id_instituicao_origem_fk` (`id_instituicao_fk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `ocorrencia`
--

INSERT INTO `ocorrencia` (`id_ocorrencia`, `id_tipo_ocorrencia_fk`, `id_instituicao_fk`, `descricao_ocorrencia`, `data_ocorrencia`, `data_cadastro_ocorrencia`, `endereco`, `latitude`, `longitude`) VALUES
(1, 1, 1, 'Foi sem querer', '2018-06-09', '2018-06-10 03:00:00', 'Rua baco 463', '-19.873462', '-43.934821'),
(2, 2, 1, 'o cara freiou do nada', '2018-06-19', '2018-06-03 03:00:00', 'aquela rua', '-19.924449', '-43.960996'),
(3, 3, 1, 'desempregados na rua', '2018-06-03', '2018-06-15 03:00:00', 'centro', '-19.913345', '-43.956316'),
(4, 1, 1, 'irembora', '0000-00-00', '2018-06-10 11:32:31', 'ruabaco 463', '-43.897905', '-19.875397');

-- --------------------------------------------------------

--
-- Estrutura da tabela `operador_instituicao`
--

CREATE TABLE IF NOT EXISTS `operador_instituicao` (
  `id_operador_inst` int(11) NOT NULL AUTO_INCREMENT,
  `id_instituicao_fk` int(11) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id_operador_inst`),
  KEY `id_instituicao_fk` (`id_instituicao_fk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_ocorrencia`
--

CREATE TABLE IF NOT EXISTS `tipo_ocorrencia` (
  `id_tipo_ocorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `descricao_tipo_ocorrencia` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo_ocorrencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tipo_ocorrencia`
--

INSERT INTO `tipo_ocorrencia` (`id_tipo_ocorrencia`, `descricao_tipo_ocorrencia`) VALUES
(1, 'Atropelamento'),
(2, 'acidente'),
(3, 'manifestacao');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `agente`
--
ALTER TABLE `agente`
  ADD CONSTRAINT `id_instituto_fk` FOREIGN KEY (`id_instituto_fk`) REFERENCES `instituicao` (`id_instituicao`);

--
-- Limitadores para a tabela `atendimento_ocorrencia`
--
ALTER TABLE `atendimento_ocorrencia`
  ADD CONSTRAINT `id_agente_fk` FOREIGN KEY (`id_agente_fk`) REFERENCES `agente` (`id_agente`),
  ADD CONSTRAINT `id_ocorrencia_fk` FOREIGN KEY (`id_ocorrencia_fk`) REFERENCES `ocorrencia` (`id_ocorrencia`);

--
-- Limitadores para a tabela `ocorrencia`
--
ALTER TABLE `ocorrencia`
  ADD CONSTRAINT `id_instituicao_origem_fk` FOREIGN KEY (`id_instituicao_fk`) REFERENCES `instituicao` (`id_instituicao`),
  ADD CONSTRAINT `id_tipo_ocorrencia_fk` FOREIGN KEY (`id_tipo_ocorrencia_fk`) REFERENCES `tipo_ocorrencia` (`id_tipo_ocorrencia`);

--
-- Limitadores para a tabela `operador_instituicao`
--
ALTER TABLE `operador_instituicao`
  ADD CONSTRAINT `id_instituicao_fk` FOREIGN KEY (`id_instituicao_fk`) REFERENCES `instituicao` (`id_instituicao`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
