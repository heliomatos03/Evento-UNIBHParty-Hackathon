<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="mycss.css">
        <link href="https://fonts.googleapis.com/css?family=Sunflower:300" rel="stylesheet">
        <script>
        function esconderNovaOcorrencia(){ 
            document.getElementsByClassName("novaocorrencia")[0].classList.add("disabled");
            document.getElementsByClassName("buttonNovaOcorrencia")[0].classList.remove("disabled");
        }
        function mostrarNovaOcorrencia(){        
            document.getElementsByClassName("buttonNovaOcorrencia")[0].classList.add("disabled");
            document.getElementsByClassName("novaocorrencia")[0].classList.remove("disabled");
        }
        function esconderAgentes(){ 
            document.getElementsByClassName("agentes")[0].classList.add("disabled");
            document.getElementsByClassName("buttonAgentes")[0].classList.remove("disabled");
        }
        function mostrarAgentes(){        
            document.getElementsByClassName("buttonAgentes")[0].classList.add("disabled");
            document.getElementsByClassName("agentes")[0].classList.remove("disabled");
        }
        function mostrarOcorrencias(){        
            document.getElementsByClassName("buttonOcorrencias")[0].classList.add("disabled");
            document.getElementsByClassName("ocorrencias")[0].classList.remove("disabled");
        }
          function esconderOcorrencias(){        
            document.getElementsByClassName("ocorrencias")[0].classList.add("disabled");
            document.getElementsByClassName("buttonOcorrencias")[0].classList.remove("disabled");
        }
        function cadastrarOcorrencia(){
        //http://localhost:8080/cop/cadastraOcorrencia.php/?id_tipo_ocorrencia_fk=1&id_instituicao_fk=1&descricao_ocorrencia=irembora&data_ocorrencia=2018-06-10&endereco=ruabaco%20463&longitude=-19.875397&latitude=-43.897905    
        //alert(document.getElementsByClassName("select_tipo")[1].options[document.getElementsByClassName("select_tipo")[1].selectedIndex].value);
        //alert(document.getElementsByClassName("select_instituicao")[2].options[document.getElementsByClassName("select_instituicao")[2].selectedIndex].value);
        //alert(document.getElementById("edit_desc_ocorrencia").value)
        alert();
    }
</script>
    </head>
    <body> 
            <div class='maincolumn' style="background-image: url('/cop/img/cabecalho.png');">
                <img src="/cop/img/logo.png" style="display:inline;"/>
                <div class="profile">Olá Ismael Reis, <a href="#">Sair</a></div>
            </div>    
        

                      
                            <style>
                              #wrapper { position: relative; }
                              #over_map { position: absolute; top: 0px; left: 0px; z-index: 99; }
                           </style>

                        <div id="wrapper">
                           <div id="google_map">
                                <?php require("mapa.html"); ?>
                           </div>
                        
                           <div id="over_map">
                               
                                <div class='ocorrencias' style="position: absolute;top:0px;left:0px;width:271px;">
                                                <div>
                                                    <span class="titlemenu">
                                                    Ocorrências</span> 
                                                    <button id="btEsconderOcorrencias" onclick='esconderOcorrencias()'>Esconder</button>
                                                </div>
                                                <div class="inputs">
                                                    <label>Instituição: 
                                                        <select  class="select_instituicao">
                                                            <option value="todas">Todas</option>
                                                            <option value="saab">COP</option>
                                                            <option value="mercedes">CICOP</option>
                                                            <option value="audi">CEMIG</option>
                                                        </select>
                                                    </label><br/>
                                                    <label>Tipo: 
                                                        <select class="select_tipo">
                                                            <option value="todos">Todos</option>
                                                            <option value="saab">Esbarroamento</option>
                                                            <option value="mercedes">Falha semáforo</option>
                                                            <option value="audi">Transito intenso</option>
                                                        </select>
                                                    </label><br/>
                                                    <label>
                                                        Estado:
                                                         <select>
                                                            <option value="volvo">Abertos e em Atendimento</option>
                                                            <option value="saab">Somente Abertos</option>
                                                            <option value="mercedes">Somente em Atendimentos</option>
                                                            <option value="audi">Atendidos</option>
                                                        </select>
                                                    </label><br/>
                                                   <!-- <label>
                                                        Data:
                                                        <input type="date"/>
                                                    </label>-->
                                                </div>
                                                <div id="listOcorrencia" class="listaOcorrencias">
                                                    
                                                </div>
                                                <div class="descricaoOcorrencia" style="text-align: center">
                                                    <div class="numOcorrencia"><h3>Ocorrência</h3> - <span id="id_ocorrencia">00</span></div>
                                                    Estado Ocorrência
                                                    <h3>Tipo de ocorrência</h3>
                                                    <span id="tipo_ocorrencia">tipo tipo tipo</span>
                                                    <div id = "descricao_ocorrencia" style="margin-left:5%;overflow: auto;max-width: 300px;max-height: 100px;text-align: justify;">Lorem ipsum dolor sit amet.</div>
                                                    <h4>Endereço Ocorrência</h4>           
                                                    <p id="endereco_ocorrencia">Endereço</p>
                                                    <button>Concluir Ocorrência</button>
                                                </div>
                                            </div>
                                        <div class="buttonOcorrencias disabled" style="position: absolute;top:0px;left:0;width:271px;" >
                                                <h3 style="display: inline">Ocorrências</h3>  <button id="btMostrarOcorrencias" onclick="mostrarOcorrencias()">Mostrar</button>
                                            </div>
                               <div class='agentes ' style="position: absolute;top:0px;left:270px;width:330px;">
                                                <div>
                                                    <span class="titlemenu">
                                                    Agentes</span> 
                                                    <button id="btEsconderAgentes" onclick='esconderAgentes()'>Esconder</button>
                                                </div>
                                                <div class="inputs">
                                                    <label>Instituição: 
                                                        <select  class="select_instituicao">
                                                            <option value="todas">Todas</option>
                                                            <option value="saab">COP</option>
                                                            <option value="mercedes">CICOP</option>
                                                            <option value="audi">CEMIG</option>
                                                        </select>
                                                    </label><br/>
                                                    <label>Identificação: 
                                                        <input type="text"/>
                                                    </label><br/>
                                                    <div><h3>Agentes próximos</h3></div>

                                                   <!-- <label>
                                                        Data:
                                                        <input type="date"/>
                                                    </label>-->
                                                </div>
                                                <div class="listaAgentes" id="agentes_proximos" style="text-align: center">
                                                    <table style="width:100%">
                                                        <tr>
                                                            <th></th>
                                                          <th>Id</th>
                                                          <th>Instituição</th>
                                                          <th>Distância</th>
                                                          <th>Ultima atualização</th>                         
                                                        </tr>
                                                        <tr>
                                                        <td><input type="checkbox"/></td>
                                                          <td>53</td>
                                                          <td>PMMG</td>
                                                          <td>10 km</td> 
                                                          <td>09/06 as 17:56</td>                  
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                          <td>54</td>
                                                          <td>PMMG</td>
                                                          <td>10 km</td> 
                                                          <td>09/06 as 17:56</td>                  
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                          <td>55</td>
                                                          <td>PMMG</td>
                                                          <td>10 km</td> 
                                                          <td>09/06 as 17:56</td>                  
                                                        </tr>
                                                      </table>
                                    
                                                </div>     
                                                   <button id="addAgente" onclick="designarAgentes()">Adicionar</button>
                                                <div class="descricaoAgentes">
                                                    <h3 class="inputs">Agentes designados</h3>
                                                     <div class="listaAgentes" id="agentesDesignados">
                                                        <table style="width:100%">
                                                        </table>                                                         
                                                    </div>
                                                    <button>Remover</button>
                                                </div>                
                                            </div>
                                            <div class="buttonAgentes disabled" style="position: absolute;top:0px;left:270px;width:330px;" >
                                                <h3 style="display: inline">Agentes</h3>  <button id="btMostrarNovaOcorrencia" onclick="mostrarAgentes()">Mostrar</button>
                                            </div>
                                            <div class='novaocorrencia disabled' style="position: absolute;top:0px;left:600px;width:350px;">
                                                <div>
                                                    <span class="titlemenu">
                                                    Nova ocorrência</span>
                                                    <button id="btEsconderNovaOcorrencia" onclick='esconderNovaOcorrencia()'>Esconder</button>
                                                </div>
                                                <div class="inputs">
                                                    <label>Instituição: 
                                                        <select class="select_instituicao">                          
                                                            <option value="saab">COP</option>
                                                            <option value="mercedes">CICOP</option>
                                                            <option value="audi">CEMIG</option>
                                                        </select>
                                                    </label><br/>
                                                    <label>Tipo: 
                                                        <select class="select_tipo">
                                                            <option value="todos">Todos</option>
                                                            <option value="saab">Esbarroamento</option>
                                                            <option value="mercedes">Falha semáforo</option>
                                                            <option value="audi">Transito intenso</option>
                                                        </select>
                                                    </label><br/>
                                                    <label>Data da ocorrência:<input type="date" id="data_ocorrencia"/></label>
                                                   <!-- <label>
                                                        Data:
                                                        <input type="date"/>
                                                    </label>-->
                                                </div>
                                                <div class="descricaoOcorrencia" style="text-align: center">             
                                                    <h1 class="inputs">Descrição:</h1>
                                                    <div><textarea id="edit_desc_ocorrencia" rows="4" cols="35" placeholder="Descreva as carecteristicas da ocorrência"></textarea> </div>
                                                    <h4>Endereço Ocorrência</h4>
                                                    <input type="text" placeholder="Ex: Rua Baco 463..."/>
                                                    <h4>Coordenadas:</h4>
                                                    <input type="text" placeholder="Latitude" size="1" id="lati"/>
                                                    <input type="text" placeholder="Longitude" size="1" id="long"/>
                                                    <button>Localizar no mapa</button>
                                                </div>
                                                <div style="text-align: right;">
                                                    <button id="botaoCadastrar" onclick="cadastrarOcorrencia();">Cadastrar</button>
                                                </div>
                                            </div>
                                            <div class="buttonNovaOcorrencia" style="position: absolute;top:0px;left:600px;width:350px;" >
                                                <h3 style="display: inline">Nova Ocorrência</h3>  <button id="btMostrarNovaOcorrencia" onclick="mostrarNovaOcorrencia()">Mostrar</button>
                                            </div>
                            </div>
                         </div>
         </div> 
    </body>
</html>
