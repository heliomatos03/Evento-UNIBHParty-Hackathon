<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="mycss.css">
        <link href="https://fonts.googleapis.com/css?family=Sunflower:300" rel="stylesheet">
    </head>
    <body style="position: relative">
                    
        
            
            <div class='maincolumn' style="background-image: url('/cop/img/cabecalho.png');">
                <img src="/cop/img/logo.png" style="display:inline;"/>
                <div class="profile">Olá Ismael Reis, <a href="#">Sair</a></div>
            </div>
      
            <div class='ocorrencias menu' style="display: inline;">
                <div>
                    <span class="titlemenu">
                    Ocorrências</span> 
                    <a href = #" >Mostrar</a>
                </div>
                <div class="inputs">
                    <label>Instituição: 
                        <select>
                            <option value="todas">Todas</option>
                            <option value="saab">COP</option>
                            <option value="mercedes">CICOP</option>
                            <option value="audi">CEMIG</option>
                        </select>
                    </label><br/>
                    <label>Tipo: 
                        <select>
                            <option value="todos">Todos</option>
                            <option value="saab">Esbarroamento</option>
                            <option value="mercedes">Falha semáforo</option>
                            <option value="audi">Transito intenso</option>
                        </select>
                    </label><br/>
                    <label>
                        Estado:
                         <select>
                            <option value="volvo">Abertos e em Atendimento</option>
                            <option value="saab">Somente Abertos</option>
                            <option value="mercedes">Somente em Atendimentos</option>
                            <option value="audi">Atendidos</option>
                        </select>
                    </label><br/>
                   <!-- <label>
                        Data:
                        <input type="date"/>
                    </label>-->
                </div>
                <div class="listaOcorrencias">
                    <table style="width:100%">
                        <tr>
                          <th>Id</th>
                          <th>Instituição</th>
                          <th>Data</th>
                          <th>Tipo</th> 
                          <th>Estado</th>
                        </tr>
                        <tr>
                          <td>55</td>
                          <td>PMMG</td>
                          <td>09/06</td> 
                          <td>Assalto</td>
                          <td>Aberto</td>
                        </tr>
                        <tr>
                          <td>56</td>
                          <td>PMMG</td>
                          <td>09/06</td> 
                          <td>Assalto</td>
                          <td>Aberto</td>
                        </tr>
                               <tr>
                          <td>56</td>
                          <td>PMMG</td>
                          <td>09/06</td> 
                          <td>Assalto</td>
                          <td>Aberto</td>
                        </tr>
                               <tr>
                          <td>56</td>
                          <td>PMMG</td>
                          <td>09/06</td> 
                          <td>Assalto</td>
                          <td>Aberto</td>
                        </tr>
                        <tr>
                          <td>57</td>
                          <td>CEMIG</td>
                          <td>09/06</td> 
                          <td>Falta de Luz</td>
                          <td>Aberto</td>
                        </tr>
                          <tr>
                          <td>57</td>
                          <td>CEMIG</td>
                          <td>09/06</td> 
                          <td>Falta de Luz</td>
                          <td>Aberto</td>
                        </tr>
                          <tr>
                          <td>57</td>
                          <td>CEMIG</td>
                          <td>09/06</td> 
                          <td>Falta de Luz</td>
                          <td>Aberto</td>
                        </tr>
                          <tr>
                          <td>57</td>
                          <td>CEMIG</td>
                          <td>09/06</td> 
                          <td>Falta de Luz</td>
                          <td>Aberto</td>
                        </tr>
                      </table>
                </div>
                <div class="descricaoOcorrencia" style="text-align: center">
                    <div class="numOcorrencia"><h3>Ocorrência</h3> - 5648</div>
                    Estado Ocorrência
                    <h3>Tipo de ocorrência</h3>
                    <div style="margin-left:5%;overflow: auto;max-width: 300px;max-height: 100px;text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eleifend dolor velit, non gravida mi scelerisque ut. Nulla convallis diam massa, eu efficitur tortor vulputate semper. Donec mattis vel ex sit amet placerat. Aliquam sed vehicula elit. Mauris egestas, eros vitae iaculis sollicitudin, sem orci dictum erat, vitae sagittis ex nisi ut purus. Duis eleifend risus quis dui convallis, non vestibulum mauris interdum. Ut lectus elit, bibendum ac auctor eu, ultrices eu dui. In sit amet nulla et lorem vehicula vehicula. Etiam in fermentum est, ac iaculis tortor.</div>
                    <h4>Endereço Ocorrência</h4>                  
                    <button>Concluir Ocorrência</button>
                </div>
            </div><div class='agentes menu'  style="display: inline;">
                <div>
                    <span class="titlemenu">
                    Agentes</span> 
                    <a href = #" >Mostrar</a>
                </div>
                <div>
                    <label>Instituição: 
                        <select>
                            <option value="todas">Todas</option>
                            <option value="saab">COP</option>
                            <option value="mercedes">CICOP</option>
                            <option value="audi">CEMIG</option>
                        </select>
                    </label><br/>
                    <label>Identificação: 
                        <input type="text"/>
                    </label><br/>
                    <div style="text-align: center">        <h3>Agentes próximos</h3></div>
            
                   <!-- <label>
                        Data:
                        <input type="date"/>
                    </label>-->
                </div>
                <div class="listaAgentes" style="text-align: center">
                    <table style="width:100%">
                        <tr>
                            <th></th>
                          <th>Id</th>
                          <th>Instituição</th>
                          <th>Distância</th>
                          <th>Ultima atualização</th>                         
                        </tr>
                        <tr>
                        <td><input type="checkbox"/></td>
                          <td>55</td>
                          <td>PMMG</td>
                          <td>10 km</td> 
                          <td>09/06 as 17:56</td>                  
                        </tr>
                        <tr>
                            <td><input type="checkbox"/></td>
                          <td>55</td>
                          <td>PMMG</td>
                          <td>10 km</td> 
                          <td>09/06 as 17:56</td>                  
                        </tr>
                        <tr>
                            <td><input type="checkbox"/></td>
                          <td>55</td>
                          <td>PMMG</td>
                          <td>10 km</td> 
                          <td>09/06 as 17:56</td>                  
                        </tr>
                      </table>
                    <button>Adicionar</button>
                </div>                
                <div class="descricaoAgentes" style="text-align: center">
                    <h1>Agentes designados</h1>
                     <div class="listaAgentes">
                        <table style="width:100%">
                            <tr>
                              <th>Id</th>
                              <th>Instituição</th>
                              <th>Distância</th>
                              <th>Ultima atualização</th>                         
                            </tr>
                            <tr>
                                <td><input type="checkbox"/></td>
                              <td>55</td>
                              <td>PMMG</td>
                              <td>10 km</td> 
                              <td>09/06 as 17:56</td>                  
                            </tr>
                            <tr>
                                <td><input type="checkbox"/></td>
                              <td>55</td>
                              <td>PMMG</td>
                              <td>10 km</td> 
                              <td>09/06 as 17:56</td>                  
                            </tr>
                            <tr>
                                <td><input type="checkbox"/></td>
                              <td>55</td>
                              <td>PMMG</td>
                              <td>10 km</td> 
                              <td>09/06 as 17:56</td>                  
                            </tr>
                          </table>
                         <button>Remover</button>
                    </div>
                </div>                
            </div>
            <div class='novaocorrencia menu' style="float: left;">
                <div>
                    <span class="titlemenu">
                    Nova ocorrência</span> 
                    <a href = #" >Mostrar</a>
                </div>
                <div>
                    <label>Instituição: 
                        <select>                          
                            <option value="saab">COP</option>
                            <option value="mercedes">CICOP</option>
                            <option value="audi">CEMIG</option>
                        </select>
                    </label><br/>
                    <label>Tipo: 
                        <select>
                            <option value="todos">Todos</option>
                            <option value="saab">Esbarroamento</option>
                            <option value="mercedes">Falha semáforo</option>
                            <option value="audi">Transito intenso</option>
                        </select>
                    </label><br/>
                  
                   <!-- <label>
                        Data:
                        <input type="date"/>
                    </label>-->
                </div>
                <div class="descricaoOcorrencia" style="text-align: center">             
                    <h1>Descrição:</h1>
                    <div><textarea rows="4" cols="50"></textarea> </div>
                    <h4>Endereço Ocorrência</h4>
                    <input type="text" placeholder="Ex: Rua Baco 463..."/>
                    <h4>Coordenadas:</h4>
                    <input type="text" placeholder="Latitude" size="1"/>
                    <input type="text" placeholder="Longitude" size="1"/>
                    <button>Localizar no mapa</button>
                </div>
                <div style="text-align: right;">
                <button>Cadastrar</button>
                </div>
            </div>
   
    <div id="map" style="width:100%;height:100%;background:#123;position: absolute;">           
            <?php print file_get_contents("http://10.61.4.215:8080/mapa/mapa.html"); ?>
    </div>
        
    </body>
</html>
